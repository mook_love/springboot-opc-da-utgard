package com.example.demo.opc.other;

import com.hotel.lot.opc.constant.OpcConstans;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.dcom.list.ClassDetails;
import org.openscada.opc.lib.list.Categories;
import org.openscada.opc.lib.list.Category;
import org.openscada.opc.lib.list.ServerList;

import java.net.UnknownHostException;
import java.util.Collection;

/**  https://www.hifreud.com/2014/12/27/opc-4-client-invoke-use-utgard/
 * @Classname ReadAllCollection
 * @Description 列举某Server下的所有OPC连接
 * @Date 2021/4/27 10:15
 * @Created by kkk
 */
public class GetAllCollection {

    public static void main(String[] args) throws JIException, UnknownHostException {
        ServerList serverList = new ServerList(OpcConstans.IP, OpcConstans.USER,
                OpcConstans.PASSWORD, "");

        Collection<ClassDetails> classDetails = serverList
                .listServersWithDetails(new Category[] {
                        Categories.OPCDAServer10, Categories.OPCDAServer20,
                        Categories.OPCDAServer30 }, new Category[] {});

        for (ClassDetails cds : classDetails) {
            System.out.println("Opc连接====="+cds.getClsId()+"=="+cds.getProgId() + " == " + cds.getDescription());
        }
    }
}
