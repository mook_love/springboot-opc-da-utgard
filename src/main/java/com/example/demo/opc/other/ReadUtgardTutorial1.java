package com.hotel.lot.opc.other;

import com.hotel.lot.opc.constant.OpcConstans;
import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.JIArray;
import org.jinterop.dcom.core.JIString;
import org.jinterop.dcom.core.JIVariant;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.*;

import java.util.concurrent.Executors;

public class ReadUtgardTutorial1 {

    public static void main(String[] args) throws Exception {
        // 连接信息
        final ConnectionInformation ci = new ConnectionInformation();
        ci.setHost(OpcConstans.IP);          // 电脑IP
        ci.setDomain("");                   // 域，为空就行
        ci.setUser(OpcConstans.USER);              // 用户名，配置DCOM时配置的
        ci.setPassword(OpcConstans.PASSWORD);           // 密码

        // 使用MatrikonOPC Server的配置
        // ci.setClsid("F8582CF2-88FB-11D0-B850-00C0F0104305"); // MatrikonOPC的注册表ID，可以在“组件服务”里看到
        // final String itemId = "u.u";    // 项的名字按实际

        // 使用KEPServer的配置
        ci.setClsid("7BC0CC8E-482C-47CA-ABDC-0FE7F9C6E729"); // KEPServer的注册表ID，可以在“组件服务”里看到
//        final String itemId = "u.u.u";    // 项的名字按实际，没有实际PLC，用的模拟器：simulator
        final String itemId = "tongdao2.shebei1.TAG5";

        // 启动服务
        final Server server = new Server(ci, Executors.newSingleThreadScheduledExecutor());

        try {
            // 连接到服务
            server.connect();
            // add sync access, poll every 500 ms，启动一个同步的access用来读取地址上的值，线程池每500ms读值一次
            // 这个是用来循环读值的，只读一次值不用这样
            final AccessBase access = new SyncAccess(server, 500);
            // 这是个回调函数，就是读到值后执行这个打印，是用匿名类写的，当然也可以写到外面去
            access.addItem(itemId, new DataCallback() {
                @Override
                public void changed(Item item, ItemState itemState) {
                    int type = 0;
                    try {
                        type = itemState.getValue().getType(); // 类型实际是数字，用常量定义的
                    } catch (JIException e) {
                        e.printStackTrace();
                    }
                    System.out.println("监控项的数据类型是：-----" + type);
                    System.out.println("监控项的时间戳是：-----" + itemState.getTimestamp().getTime());
                    System.out.println("监控项的详细信息是：-----" + itemState);

                    // 如果读到是short类型的值
                    if (type == JIVariant.VT_I2) {
                        short n = 0;
                        try {
                            n = itemState.getValue().getObjectAsShort();
                        } catch (JIException e) {
                            e.printStackTrace();
                        }
                        System.out.println("-----short类型值： " + n);
                    }
                    //如果读到是World类型的值
                    if (type == JIVariant.VT_I4) {
                        long n = 0;
                        try {
                            n = itemState.getValue().getObjectAsShort();
                        } catch (JIException e) {
                            e.printStackTrace();
                        }
                        System.out.println("-----long类型值： " + n);
                    }
                    //如果读到是World类型的值
                    if (type == JIVariant.VT_UI2) {
                        Number n = 0;
                        try {
                            n = itemState.getValue().getObjectAsShort();
                        } catch (JIException e) {
                            e.printStackTrace();
                        }
                        System.out.println("-----world类型值： " + n);
                    }
                    //如果读到是Float类型的值
                    if (type == JIVariant.VT_R4) {
                        float n = 0;
                        try {
                            n = itemState.getValue().getObjectAsShort();
                        } catch (JIException e) {
                            e.printStackTrace();
                        }
                        System.out.println("-----float类型值： " + n);
                    }
                    // 如果读到是字符串类型的值
                    if (type == JIVariant.VT_BSTR) {  // 字符串的类型是8
                        JIString value = null;
                        try {
                            value = itemState.getValue().getObjectAsString();
                        } catch (JIException e) {
                            e.printStackTrace();
                        } // 按字符串读取
                        String str = value.getString(); // 得到字符串
                        System.out.println("-----String类型值： " + str);
                    }
                    // 读取Float类型的数组
                    if (type == 8194) { // 8196是打印state.getValue().getType()得到的
                        JIArray jarr = null; // 按数组读取
                        try {
                            jarr = itemState.getValue().getObjectAsArray();
                        } catch (JIException e) {
                            e.printStackTrace();
                        }
//                        Object arrayInstance = jarr.getArrayInstance();
                        Object[] le = (Object[]) jarr.getArrayInstance();
//                        short[] arr =  (short[])jarr.getArrayInstance();  // 得到数组
                        String value = "";
                        for (Object f : le) {
                            value = value + f + ",";
                        }
                        System.out.println("-----数组类型值： " + value.substring(0, value.length() - 1)); // 遍历打印数组的值，中间用逗号分隔，去掉最后逗号
                    }
                }
            });
            // start reading，开始读值
            access.bind();
            // wait a little bit，有个10秒延时
            Thread.sleep(10 * 1000);
            // stop reading，停止读取
            access.unbind();
        } catch (final JIException e) {
            System.out.println(String.format("%08X: %s", e.getErrorCode(), server.getErrorMessage(e.getErrorCode())));
        }
    }
}