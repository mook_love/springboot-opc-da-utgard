package com.example.demo.opc.other;

import com.hotel.lot.opc.constant.OpcConstans;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.dcom.list.ClassDetails;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.Server;
import org.openscada.opc.lib.da.browser.Branch;
import org.openscada.opc.lib.da.browser.FlatBrowser;
import org.openscada.opc.lib.da.browser.Leaf;
import org.openscada.opc.lib.list.Categories;
import org.openscada.opc.lib.list.Category;
import org.openscada.opc.lib.list.ServerList;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.concurrent.Executors;

/**
 * @Classname ReadAllCollection
 * @Description 列举连接下的所有Group和Item
 * @Date 2021/4/27 10:15
 * @Created by kkk
 */
public class GetAllGroupAndItem {

    public static void main(String[] args) throws JIException, UnknownHostException, AlreadyConnectedException {
        ConnectionInformation ci = new ConnectionInformation();
        ci.setHost(OpcConstans.IP);
        ci.setDomain("");
        ci.setUser(OpcConstans.USER);
        ci.setPassword(OpcConstans.PASSWORD);
        ci.setClsid(OpcConstans.CLSID);

        Server server = new Server(ci, Executors.newSingleThreadScheduledExecutor());

        server.connect();

        dumpTree(server.getTreeBrowser().browse(), 0);
        dumpFlat(server.getFlatBrowser());

        server.disconnect();
    }

    private static void dumpFlat(final FlatBrowser browser)
            throws IllegalArgumentException, UnknownHostException, JIException {
        for (String name : browser.browse()) {
            System.out.println(name);
        }
    }

    private static void dumpTree(final Branch branch, final int level) {

        for (final Leaf leaf : branch.getLeaves()) {
            dumpLeaf(leaf, level);
        }
        for (final Branch subBranch : branch.getBranches()) {
            dumpBranch(subBranch, level);
            dumpTree(subBranch, level + 1);
        }
    }

    private static String printTab(int level) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level; i++) {
            sb.append("\t");
        }
        return sb.toString();
    }

    private static void dumpLeaf(final Leaf leaf, final int level) {
        System.out.println(printTab(level) + "Leaf: " + leaf.getName() + ":"
                + leaf.getItemId());
    }

    private static void dumpBranch(final Branch branch, final int level) {
        System.out.println(printTab(level) + "Branch: " + branch.getName());
    }
}