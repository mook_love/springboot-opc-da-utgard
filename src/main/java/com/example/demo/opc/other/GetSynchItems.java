package com.example.demo.opc.other;

import com.hotel.lot.opc.constant.OpcConstans;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.Group;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.Server;

import java.util.Map;
import java.util.concurrent.Executors;

/**
 * @Classname GetSynchItems
 * @Description Item的同步查询  word类型需要自己转换
 * @Date 2021/4/27 10:40
 * @Created by kkk
 */
public class GetSynchItems {
    public static void main(String[] args) throws Exception {

        ConnectionInformation ci = new ConnectionInformation();
        ci.setHost(OpcConstans.IP);
        ci.setDomain("");
        ci.setUser(OpcConstans.USER);
        ci.setPassword(OpcConstans.PASSWORD);
        ci.setClsid(OpcConstans.CLSID);

        Server server = new Server(ci,Executors.newSingleThreadScheduledExecutor());

        server.connect();

        Group group = server.addGroup();
        Item item = group.addItem("tongdao2.shebei1.TAG5");

        Map<String, Item> items = group.addItems("tongdao2.shebei1.TAG5",
                "tongdao2.shebei1.TAG1", "tongdao2.shebei1.TAG2", "tongdao2.shebei1.TAG3");

        dumpItem(item);

        for (Map.Entry<String, Item> temp : items.entrySet()) {
            dumpItem(temp.getValue());
        }
        server.dispose();
    }

    private static void dumpItem(Item item) throws JIException {
        System.out.println("[" + (++count) + "],ItemName:[" + item.getId()
                + "],value:" + item.read(false).getValue());
    }

    private static int count;
}
