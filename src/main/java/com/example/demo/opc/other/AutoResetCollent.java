package com.example.demo.opc.other;

import com.hotel.lot.opc.constant.OpcConstans;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.*;

import java.util.concurrent.Executors;

/**
 * @Classname AutoResetCollent
 * @Description 自动重连Item异步读取
 * @Date 2021/4/27 12:20
 * @Created by kkk
 */
public class AutoResetCollent {

    private static final int PERIOD = 100;

    private static final int SLEEP = 2000;

    public static void main(String[] args) throws Exception {

        ConnectionInformation ci = new ConnectionInformation();
        ci.setHost(OpcConstans.IP);
        ci.setDomain("");
        ci.setUser(OpcConstans.USER);
        ci.setPassword(OpcConstans.PASSWORD);
        ci.setClsid(OpcConstans.CLSID);

        Server server = new Server(ci, Executors.newSingleThreadScheduledExecutor());

        AutoReconnectController controller = new AutoReconnectController(server);

        controller.connect();

        AccessBase access = new SyncAccess(server, PERIOD);

        access.addItem("tongdao2.shebei1.TAG5", new DataCallback() {
            private int i;

            public void changed(Item item, ItemState itemstate) {
                System.out.println("[" + (++i) + "],ItemName:[" + item.getId()
                        + "],value:" + itemstate.getValue());
            }
        });

        access.bind();
        Thread.sleep(SLEEP);
        access.unbind();
        controller.disconnect();
    }
}
