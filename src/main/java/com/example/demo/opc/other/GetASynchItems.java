package com.example.demo.opc.other;

import com.hotel.lot.opc.constant.OpcConstans;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.*;

import java.util.concurrent.Executors;

/**
 * @Classname GetASynchItems
 * @Description Item的异步查询
 * @Date 2021/4/27 10:46
 * @Created by kkk
 */
public class GetASynchItems {
    private static final int PERIOD = 100;

    private static final int SLEEP = 2000;

    public static void main(String[] args) throws Exception {

        ConnectionInformation ci = new ConnectionInformation();
        ci.setHost(OpcConstans.IP);
        ci.setDomain("");
        ci.setUser(OpcConstans.USER);
        ci.setPassword(OpcConstans.PASSWORD);
        ci.setClsid(OpcConstans.CLSID);

        Server server = new Server(ci, Executors.newSingleThreadScheduledExecutor());
        server.connect();
        AccessBase access = new SyncAccess(server, PERIOD);//线程数
        access.addItem("tongdao2.shebei1.TAG5", new DataCallback() {
            private int i;

            public void changed(Item item, ItemState itemstate) {
                System.out.println("[" + (++i) + "],ItemName:[" + item.getId()
                        + "],value:" + itemstate.getValue());
            }
        });
        access.bind();
        Thread.sleep(SLEEP);
        access.unbind();
        server.dispose();
    }
}
