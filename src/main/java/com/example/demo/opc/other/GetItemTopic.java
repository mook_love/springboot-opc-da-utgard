package com.example.demo.opc.other;

import com.hotel.lot.opc.constant.OpcConstans;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.*;

import java.util.concurrent.Executors;

/**
 * @Classname GetItemTopic
 * @Description Item的发布订阅查询
 * @Date 2021/4/27 10:50
 * @Created by kkk
 */
public class GetItemTopic {
    //TODO 订阅失败org.jinterop.dcom.common.JIException:
    //The RPC server is unavailable. Please check if the COM server is up and running and that route to the COM Server is accessible
    //(A simple "Ping" to the Server machine would do). Also please confirm if the Windows Firewall is not blocking DCOM access. [0x800706BA]
    private static final int PERIOD = 100;

    private static final int SLEEP = 5000;

    public static void main(String[] args) throws Exception {

        ConnectionInformation ci = new ConnectionInformation();
        ci.setHost(OpcConstans.IP);
        ci.setDomain("");
        ci.setUser(OpcConstans.USER);
        ci.setPassword(OpcConstans.PASSWORD);
        ci.setClsid(OpcConstans.CLSID);

        Server server = new Server(ci, Executors.newSingleThreadScheduledExecutor());

        server.connect();

        AccessBase  access = new Async20Access(server, PERIOD, false);

        access.addItem("tongdao2.shebei1.TAG5", new DataCallback() {

            private int count;

            public void changed(Item item, ItemState itemstate) {
                System.out.println("[" + (++count) + "],ItemName:["
                        + item.getId() + "],value:" + itemstate.getValue());
            }
        });

        access.bind();
        Thread.sleep(SLEEP);
        access.unbind();
        server.dispose();
    }

}
