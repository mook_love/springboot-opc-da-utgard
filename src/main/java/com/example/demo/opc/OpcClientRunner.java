package com.example.demo.opc;

import com.example.demo.opc.config.OpcClientConfigBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**    https://blog.csdn.net/yhtppp/article/details/80680446
 * @Classname OpcClientRunner
 * @Description TODO
 * @Date 2021/4/27 19:51
 * @Created by kkk
 */
@Slf4j
@Component
@Order(value = 1)//这里表示启动顺序
public class OpcClientRunner implements CommandLineRunner {
    @Autowired
    private OpcClientBean opcClientBean;
    @Autowired
    private OpcClientConfigBean clientConfigBean;

    @Override
    public void run(String... args) throws Exception {
        log.info("OpcClientRunner  starting ...........");
        opcClientBean.start(
                clientConfigBean.getIp(),
                clientConfigBean.getUser(),clientConfigBean.getPassword(),
                clientConfigBean.getDomain(),clientConfigBean.getClsid());
    }
}
