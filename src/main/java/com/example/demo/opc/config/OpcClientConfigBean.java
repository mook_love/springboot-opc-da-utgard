package com.example.demo.opc.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Classname OpcClientConfigBean
 * @Description OPC配置信息
 * @Date 2021/4/27 19:45
 * @Created by kkk
 */

@Configuration
@ConfigurationProperties(prefix = "opc")
@Data
public class OpcClientConfigBean {
    private String ip;
    private String clsid;
    private String user;
    private String password;
    private String domain;


}
