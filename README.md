# springboot-opc-Utgard

#### 介绍
Java实现OPC通信 采用utgard方式，JeasyOPC太操蛋，不用他

#### springboot-opc-Utgard软件架构
软件架构说明
    使用springboot实现opc接入，

#### 安装教程

JeasyOPC 只支持32jdk 而且不能跨平台，老掉牙的东西，很难用就不深究了
本文采用： utgard方式 加观察者模式实现OPC消息订阅

#### 注意要点：
1.没有opc服务器、自己搭建链接，采用KEPServerEX 6 Configuration工具模拟opc服务器数据，工具下载地址： https://pan.baidu.com/share/init?surl=3Bc0gfGxCWo8fddTt-ut-w 密码：ykj2 工具按照一路默认next就行 参考博客大佬地址：https://www.cnblogs.com/ioufev/p/9928971.html

2.视频教程：https://www.bilibili.com/video/BV13V411f7Ch/ 

3.注意严格按照步骤进行OPC和DCOM配置，否则出现一些奇怪从错误！ 可以用《OPC运行时环境一键配置.zip》先尝试一键配置环境，如若不行：则采用手动配置DCOM，尽量也参考本文上传到doc文件的 《正版：Win7和Win7_SP1网络OPC配置完整版.pdf》《英文：Remote OPC DA - Quick Start Guide (DCOM).pdf》 《辅助：OPC远程连接配置1111(1).docx》

4.other 文件夹里面demo可作为参考进行拓展 参考博客：https://www.hifreud.com/2014/12/27/opc-4-client-invoke-use-utgard/

#### 使用说明

1.  若有server课忽略，若没有：请先查看：https://www.cnblogs.com/ioufev/p/9928971.html 地址 安装KEPServerEX 6  模拟opcServer，
2.  若有server课忽略，支持win10专业版，其他系统请升级专业版进行PC和DCOM配置

3.注意通道级别
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/121051_d5b49da6_1948025.png "屏幕截图.png")
 final String itemId = "tongdao2.shebei1.TAG4"; //代表通道2下的shebei1组 下的 TAG4数据
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/121221_97a05df1_1948025.png "屏幕截图.png")

#### 项目目录
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/121301_006ecd91_1948025.png "屏幕截图.png")



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
