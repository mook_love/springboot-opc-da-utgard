# springboot-opc-Utgard

#### Description
JeasyOPC 只支持32jdk 而且不能跨平台，老掉牙的东西，很难用就不深究了

本文采用： utgard方式 加观察者模式实现OPC消息订阅
### 注意要点：
1. 没有opc服务器、自己搭建链接，采用KEPServerEX 6 Configuration工具模拟opc服务器数据，工具下载地址：
   https://pan.baidu.com/share/init?surl=3Bc0gfGxCWo8fddTt-ut-w  密码：ykj2  工具按照一路默认next就行
   参考博客大佬地址：https://www.cnblogs.com/ioufev/p/9928971.html
   
2.视频教程：https://www.bilibili.com/video/BV13V411f7Ch/
3.注意严格按照步骤进行OPC和DCOM配置，否则出现一些奇怪从错误！ 
可以用《OPC运行时环境一键配置.zip》先尝试一键配置环境，如若不行：则采用手动配置DCOM，尽量也参考本文上传到doc文件的
《正版：Win7和Win7_SP1网络OPC配置完整版.pdf》《英文：Remote OPC DA - Quick Start Guide (DCOM).pdf》
 《辅助：OPC远程连接配置1111(1).docx》

4. other 文件夹里面demo可作为参考进行拓展
参考博客：https://www.hifreud.com/2014/12/27/opc-4-client-invoke-use-utgard/

#### Software Architecture
Software architecture description
